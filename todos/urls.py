from django.urls import path
from .views import todo_list, todo_list_detail


urlpatterns=[
    path("tasks", todo_list, name= "todo_list_list"),
    path("<int:id>/", todo_list_detail, name= "todo_list_detail"),
]
from django.shortcuts import render, get_object_or_404
from .models import TodoList

# Create your views here.
# def todo_list(request):

def todo_list(request):
    todos = TodoList.objects.all()
    context = {"todo_list": todos}
    return render(request, "html/list.html", context)
    
 
def todo_list_detail(request, id):
    todo_details = get_object_or_404(TodoList, id = id)
    context = {
        "todo_object": todo_details,
        }
    return render(request, "html/details.html", context)

